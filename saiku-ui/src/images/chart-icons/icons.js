/*
 *   Copyright 2012-present OSBI Ltd
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

// Icons
import barIconDefault from './default/bar.png';
import barIconActive from './active/bar.png';

import stackedBarIconDefault from './default/stackedbar.png';
import stackedBarIconActive from './active/stackedbar.png';

import stackedBar100IconDefault from './default/100bar.png';
import stackedBar100IconActive from './active/100bar.png';

import multipleIconDefault from './default/multiple.png';
import multipleIconActive from './active/multiple.png';

import lineIconDefault from './default/line.png';
import lineIconActive from './active/line.png';

import areaIconDefault from './default/area.png';
import areaIconActive from './active/area.png';

import heatGridIconDefault from './default/area2.png';
import heatGridIconActive from './active/area2.png';

import treeMapIconDefault from './default/heat.png';
import treeMapIconActive from './active/heat.png';

import sunburstIconDefault from './default/sunburst.png';
import sunburstIconActive from './active/sunburst.png';

import multipleSunburstIconDefault from './default/multiplesunburst.png';
import multipleSunburstIconActive from './active/multiplesunburst.png';

import dotIconDefault from './default/dot.png';
import dotIconActive from './active/dot.png';

import waterfallIconDefault from './default/waterfall.png';
import waterfallIconActive from './active/waterfall.png';

import pieIconDefault from './default/pie.png';
import pieIconActive from './active/pie.png';

export {
  barIconDefault,
  barIconActive,
  stackedBarIconDefault,
  stackedBarIconActive,
  stackedBar100IconDefault,
  stackedBar100IconActive,
  multipleIconDefault,
  multipleIconActive,
  lineIconDefault,
  lineIconActive,
  areaIconDefault,
  areaIconActive,
  heatGridIconDefault,
  heatGridIconActive,
  treeMapIconDefault,
  treeMapIconActive,
  sunburstIconDefault,
  sunburstIconActive,
  multipleSunburstIconDefault,
  multipleSunburstIconActive,
  dotIconDefault,
  dotIconActive,
  waterfallIconDefault,
  waterfallIconActive,
  pieIconDefault,
  pieIconActive
};
