/*
 *   Copyright 2012-present OSBI Ltd
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

// Icons
import {
  barIconDefault,
  barIconActive,
  stackedBarIconDefault,
  stackedBarIconActive,
  stackedBar100IconDefault,
  stackedBar100IconActive,
  multipleIconDefault,
  multipleIconActive,
  lineIconDefault,
  lineIconActive,
  areaIconDefault,
  areaIconActive,
  heatGridIconDefault,
  heatGridIconActive,
  treeMapIconDefault,
  treeMapIconActive,
  sunburstIconDefault,
  sunburstIconActive,
  multipleSunburstIconDefault,
  multipleSunburstIconActive,
  dotIconDefault,
  dotIconActive,
  waterfallIconDefault,
  waterfallIconActive,
  pieIconDefault,
  pieIconActive
} from './icons';

export const chartItems = [
  {
    id: 'bar',
    title: 'Bar',
    pathDefault: barIconDefault,
    pathActive: barIconActive,
    active: false,
    disabled: false
  },
  {
    id: 'stackedBar',
    title: 'Stacked Bar',
    pathDefault: stackedBarIconDefault,
    pathActive: stackedBarIconActive,
    active: false,
    disabled: false
  },
  {
    id: 'stackedBar100',
    title: 'Bar 100%',
    pathDefault: stackedBar100IconDefault,
    pathActive: stackedBar100IconActive,
    active: false,
    disabled: false
  },
  {
    id: 'multiplebar',
    title: 'Multiple Bar',
    pathDefault: multipleIconDefault,
    pathActive: multipleIconActive,
    active: false,
    disabled: false
  },
  {
    id: 'line',
    title: 'Line',
    pathDefault: lineIconDefault,
    pathActive: lineIconActive,
    active: false,
    disabled: false
  },
  {
    id: 'area',
    title: 'Area',
    pathDefault: areaIconDefault,
    pathActive: areaIconActive,
    active: false,
    disabled: false
  },
  {
    id: 'heatgrid',
    title: 'Heat Grid',
    pathDefault: heatGridIconDefault,
    pathActive: heatGridIconActive,
    active: false,
    disabled: false
  },
  {
    id: 'treemap',
    title: 'Tree Map',
    pathDefault: treeMapIconDefault,
    pathActive: treeMapIconActive,
    active: false,
    disabled: false
  },
  {
    id: 'sunburst',
    title: 'Sunburst',
    pathDefault: sunburstIconDefault,
    pathActive: sunburstIconActive,
    active: false,
    disabled: false
  },
  {
    id: 'multiplesunburst',
    title: 'Multi Sunburst',
    pathDefault: multipleSunburstIconDefault,
    pathActive: multipleSunburstIconActive,
    active: false,
    disabled: false
  },
  {
    id: 'dot',
    title: 'Dot',
    pathDefault: dotIconDefault,
    pathActive: dotIconActive,
    active: false,
    disabled: false
  },
  {
    id: 'waterfall',
    title: 'Waterfall',
    pathDefault: waterfallIconDefault,
    pathActive: waterfallIconActive,
    active: false,
    disabled: false
  },
  {
    id: 'pie',
    title: 'Pie',
    pathDefault: pieIconDefault,
    pathActive: pieIconActive,
    active: false,
    disabled: false
  }
];
