/*
 *   Copyright 2012-present OSBI Ltd
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

// Types
import { REQUEST_START, REQUEST_SUCCESS, REQUEST_FAILURE } from './types';

export const requestStart = message => {
  return {
    type: REQUEST_START,
    payload: message
  };
};

export const requestSuccess = () => {
  return {
    type: REQUEST_SUCCESS
  };
};

export const requestFailure = () => {
  return {
    type: REQUEST_FAILURE
  };
};
