/*
 *   Copyright 2012-present OSBI Ltd
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

// Actions
import * as blockUiActions from './blockUiActions';
import * as datasourcesActions from './datasourcesActions';
import * as queryActions from './queryActions';
import * as sessionActions from './sessionActions';
import * as workspaceActions from './workspaceActions';

export const actionCreators = Object.assign(
  {},
  blockUiActions,
  datasourcesActions,
  queryActions,
  sessionActions,
  workspaceActions
);
