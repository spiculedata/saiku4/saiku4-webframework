/*
 *   Copyright 2012-present OSBI Ltd
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

// Types
export const LOADING_SESSION = 'LOADING_SESSION';
export const SET_CURRENT_SESSION = 'SET_CURRENT_SESSION';
export const LOADING_LICENSE = 'LOADING_LICENSE';
export const SET_CURRENT_LICENSE = 'SET_CURRENT_LICENSE';
export const LOADING_DATASOURCES = 'LOADING_DATASOURCES';
export const SET_DATASOURCES = 'SET_DATASOURCES';
export const ADD_CUBE = 'ADD_CUBE';
export const SET_CUBE = 'SET_CUBE';
export const SAVE_SELECTED_CUBE = 'SAVE_SELECTED_CUBE';
export const OLAP_QUERY_INIT = 'OLAP_QUERY_INIT';
export const OLAP_QUERY_RESULT = 'OLAP_QUERY_RESULT';
export const SET_QUERY_PROPERTY = 'SET_QUERY_PROPERTY';
export const QUERY_RESULTSET = 'QUERY_RESULTSET';
export const QUERY_RESULTSET_ERROR = 'QUERY_RESULTSET_ERROR';
export const SAVE_QUERY = 'SAVE_QUERY';
export const SAVE_OPEN_QUERY = 'SAVE_OPEN_QUERY';
export const SAVE_QUERY_DESIGNER = 'SAVE_QUERY_DESIGNER';
export const SWITCH_RENDER = 'SWITCH_RENDER';
export const REQUEST_START = 'REQUEST_START';
export const REQUEST_SUCCESS = 'REQUEST_SUCCESS';
export const REQUEST_FAILURE = 'REQUEST_FAILURE';
export const RESET_QUERY = 'RESET_QUERY';
export const RESET_STATE = 'RESET_STATE';
export const GET_ERRORS = 'GET_ERRORS';
export const CLEAR_ERRORS = 'CLEAR_ERRORS';
