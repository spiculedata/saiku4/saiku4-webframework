/*
 *   Copyright 2012-present OSBI Ltd
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

// Packages
import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';

// Reducers
import { blockUiReducer } from './blockUiReducer';
import { datasourcesReducer } from './datasourcesReducer';
import { errorReducer } from './errorReducer';
import { licenseReducer } from './licenseReducer';
import { queryReducer } from './queryReducer';
import { queryResultsetReducer } from './queryResultsetReducer';
import { sessionReducer } from './sessionReducer';
import { workspaceReducer } from './workspaceReducer';

export default history =>
  combineReducers({
    blockUi: blockUiReducer,
    datasources: datasourcesReducer,
    errors: errorReducer,
    licenseInfo: licenseReducer,
    query: queryReducer,
    queryResultset: queryResultsetReducer,
    router: connectRouter(history),
    session: sessionReducer,
    workspace: workspaceReducer
  });
