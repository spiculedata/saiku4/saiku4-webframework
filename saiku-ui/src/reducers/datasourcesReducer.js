/*
 *   Copyright 2012-present OSBI Ltd
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

// Types
import {
  LOADING_DATASOURCES,
  SET_DATASOURCES,
  ADD_CUBE,
  RESET_STATE
} from '../actions/types';

const initialState = {
  datasources: [],
  cubes: [],
  loading: false
};

export const datasourcesReducer = (state = initialState, action) => {
  switch (action.type) {
    case LOADING_DATASOURCES:
      return {
        ...state,
        loading: true
      };
    case SET_DATASOURCES:
      return {
        ...state,
        datasources: action.payload,
        loading: false
      };
    case ADD_CUBE:
      return {
        ...state,
        cubes: state.cubes.concat(action.payload),
        loading: false
      };
    case RESET_STATE:
      return initialState;
    default:
      return state;
  }
};
