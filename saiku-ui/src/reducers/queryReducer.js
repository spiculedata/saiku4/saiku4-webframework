/*
 *   Copyright 2012-present OSBI Ltd
 *
 *   Licensed under the Apache License, Version 2.0 (the 'License');
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an 'AS IS' BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

// Packages
import uuid from 'uuid/v4';

// Utils
import { Settings } from '../utils';

// Types
import {
  SET_CUBE,
  OLAP_QUERY_INIT,
  OLAP_QUERY_RESULT,
  SET_QUERY_PROPERTY,
  RESET_QUERY,
  RESET_STATE
} from '../actions/types';

const initialState = {
  query: {
    name: uuid().toUpperCase(),
    queryModel: {
      axes: {
        FILTER: {
          mdx: null,
          filters: [],
          sortOrder: null,
          sortEvaluationLiteral: null,
          hierarchizeMode: null,
          location: 'FILTER',
          hierarchies: [],
          nonEmpty: false
        },
        COLUMNS: {
          mdx: null,
          filters: [],
          sortOrder: null,
          sortEvaluationLiteral: null,
          hierarchizeMode: null,
          location: 'COLUMNS',
          hierarchies: [],
          nonEmpty: true
        },
        ROWS: {
          mdx: null,
          filters: [],
          sortOrder: null,
          sortEvaluationLiteral: null,
          hierarchizeMode: null,
          location: 'ROWS',
          hierarchies: [],
          nonEmpty: true
        }
      },
      visualTotals: false,
      visualTotalsPattern: null,
      lowestLevelsOnly: false,
      details: {
        axis: 'COLUMNS',
        location: 'BOTTOM',
        measures: []
      },
      calculatedMeasures: [],
      calculatedMembers: []
    },
    queryType: 'OLAP',
    type: 'QUERYMODEL'
  },
  loading: false
};

export const queryReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_CUBE:
      return {
        ...state,
        query: {
          ...state.query,
          name: uuid().toUpperCase(),
          ...action.payload
        }
      };
    case OLAP_QUERY_INIT:
      return {
        ...state,
        query: {
          ...state.query,
          ...action.payload,
          properties: {
            ...state.query.properties,
            ...Settings.QUERY_PROPERTIES,
            ...action.payload.properties
          }
        }
      };
    case OLAP_QUERY_RESULT:
      return {
        ...state,
        query: Object.assign({}, action.payload)
      };
    case SET_QUERY_PROPERTY:
      return {
        ...state,
        query: {
          ...state.query,
          properties: {
            ...state.query.properties,
            ...action.payload
          }
        }
      };
    case RESET_QUERY:
      const { cube } = state.query;

      return {
        ...initialState,
        query: {
          ...initialState.query,
          cube
        }
      };
    case RESET_STATE:
      return initialState;
    default:
      return state;
  }
};
