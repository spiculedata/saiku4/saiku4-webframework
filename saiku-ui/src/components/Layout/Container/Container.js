/*
 *   Copyright 2012-present OSBI Ltd
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

// Packages
import React from 'react';
import PropTypes from 'prop-types';
import { matchPath, withRouter } from 'react-router-dom';

// Routes
import { routes } from '../../../routes';

// Common
import { ErrorBoundary, KeyboardShortcuts } from '../../Common';

// Elements
import { PageTitle, Sidebar, Toolbar } from '../../Elements';

// Styles
import './Container.css';

const Container = ({ location, children }) => {
  const showSidebar = () => {
    for (let route of routes) {
      if (matchPath(location.pathname, route)) {
        return route.showSidebar;
      }
    }

    return false;
  };

  return (
    <ErrorBoundary>
      <KeyboardShortcuts>
        <div className="page-inner">
          <Toolbar />
          {showSidebar() && <Sidebar />}

          <div className="main">
            {showSidebar() && <PageTitle />}
            <div className="content">{children}</div>
          </div>
        </div>
      </KeyboardShortcuts>
    </ErrorBoundary>
  );
};

Container.propTypes = {
  children: PropTypes.node.isRequired
};

export default withRouter(Container);
