/*
 *   Copyright 2012-present OSBI Ltd
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

// Packages
import React, { PureComponent, Fragment } from 'react';
import mouseTrap from 'react-mousetrap';

// Dialogs
import { KeyboardShortcutsDialog } from '../Dialogs';

class KeyboardShortcuts extends PureComponent {
  state = {
    isOpenKeyboardShortcutsDialog: false
  };

  componentDidMount() {
    this.props.bindShortcut('mod+k', () => {
      this.handleKeyboardShortcutsDialog();

      return false;
    });
  }

  handleKeyboardShortcutsDialog = () => {
    this.setState(prevState => ({
      isOpenKeyboardShortcutsDialog: !prevState.isOpenKeyboardShortcutsDialog
    }));
  };

  render() {
    const { isOpenKeyboardShortcutsDialog } = this.state;

    return (
      <Fragment>
        {this.props.children}

        {isOpenKeyboardShortcutsDialog && (
          <KeyboardShortcutsDialog
            onClose={this.handleKeyboardShortcutsDialog}
          />
        )}
      </Fragment>
    );
  }
}

export default mouseTrap(KeyboardShortcuts);
