/*
 *   Copyright 2012-present OSBI Ltd
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

// Packages
import React from 'react';
import { withRouter } from 'react-router-dom';
import { Button, NonIdealState } from '@blueprintjs/core';

// Layout
import Empty from '../../Layout/Empty';

const NoMatch = ({ history }) => {
  const btnGoBack = (
    <Button
      icon="chevron-left"
      text="Go Back"
      intent="primary"
      onClick={() => history.goBack()}
      minimal
    />
  );

  return (
    <Empty showWaterMark>
      <div className="sku-page-empty-container">
        <NonIdealState
          icon="error"
          title="404 ERROR"
          description="Page not found!"
          action={btnGoBack}
        />
      </div>
    </Empty>
  );
};

export default withRouter(NoMatch);
