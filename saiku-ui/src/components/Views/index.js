/*
 *   Copyright 2012-present OSBI Ltd
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

// Views
import AdminConsole from './AdminConsole';
import AutoLogin from './AutoLogin';
import GeneralSettings from './GeneralSettings';
import Home from './Home';
import Login from './Login';
import NoMatch from './NoMatch';
import QueryManager from './QueryManager';
import Workspace from './Workspace';

export {
  AdminConsole,
  AutoLogin,
  GeneralSettings,
  Home,
  Login,
  NoMatch,
  QueryManager,
  Workspace
};
