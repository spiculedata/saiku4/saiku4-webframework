/*
 *   Copyright 2012-present OSBI Ltd
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

// Packages
import React, { Component } from 'react';
import ReactFlagsSelect from 'react-flags-select';
import { FormGroup } from '@blueprintjs/core';

// Layout
import Container from '../../Layout/Container';

// Utils
import { Settings } from '../../../utils';

// Styles
import 'react-flags-select/css/react-flags-select.css';
import './GeneralSettings.css';

class GeneralSettings extends Component {
  render() {
    return (
      <Container>
        <div className="sku-general-settings content-inner">
          <div className="sku-settings-content">
            <FormGroup label="Language" labelFor="language" inline>
              <ReactFlagsSelect
                className="sku-menu-flags"
                countries={Settings.LANGUAGES}
                defaultCountry="GB"
                placeholder="Select Language"
                showSelectedLabel={false}
                showOptionLabel={false}
                onSelect={value => console.log(value)}
              />
            </FormGroup>
          </div>
        </div>
      </Container>
    );
  }
}

export default GeneralSettings;
