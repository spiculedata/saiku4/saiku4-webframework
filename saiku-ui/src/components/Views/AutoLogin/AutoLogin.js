/*
 *   Copyright 2012-present OSBI Ltd
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

// Packages
import { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { isEmpty } from 'lodash';

// Actions
import { actionCreators } from '../../../actions';

// Utils
import { Settings } from '../../../utils';

class AutoLogin extends Component {
  componentWillMount() {
    this.showSplashScreen();
    this.autoLogin();
  }

  componentDidMount() {
    const { isAuthenticated } = this.props;

    if (isAuthenticated) {
      this.handleAuthentication();
    }
  }

  componentWillReceiveProps(nextProps) {
    const { isAuthenticated, errors } = nextProps;

    if (isAuthenticated) {
      this.handleAuthentication();
    }

    if (!isEmpty(errors)) {
      alert(errors);
    }
  }

  showSplashScreen() {
    const $el = document.getElementById('sku-progress-indicator');

    $el.removeAttribute('hidden');
  }

  handleAuthentication() {
    const { push } = this.props.history;

    push(Settings.DEFAULT_ROUTE);
  }

  autoLogin() {
    this.props.doLogin(
      Settings.AUTO_LOGIN.USERNAME,
      Settings.AUTO_LOGIN.PASSWORD
    );
  }

  render() {
    return null;
  }
}

AutoLogin.propTypes = {
  isAuthenticated: PropTypes.bool.isRequired,
  errors: PropTypes.oneOfType([PropTypes.object, PropTypes.string]),
  doLogin: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  ...state.session,
  errors: state.errors
});

const mapDispatchToProps = dispatch => ({
  doLogin: (username, password) =>
    dispatch(actionCreators.doLogin(username, password))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AutoLogin);
