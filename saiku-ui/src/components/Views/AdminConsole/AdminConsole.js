/*
 *   Copyright 2012-present OSBI Ltd
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

// Packages
import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import ReduxBlockUi from 'react-block-ui/lib/redux';
import { Collapse } from 'antd';

// Layout
import Container from '../../Layout/Container';

// Panels
import {
  PanelDataSourcesManagement,
  PanelLicenseManagement,
  PanelLicenseUserManagement,
  PanelSchemasManagement,
  PanelUserManagement
} from './Panels';

// Utils
import {
  REQUEST_START,
  REQUEST_SUCCESS,
  REQUEST_FAILURE
} from '../../../utils/constants';

// Components
const { Panel } = Collapse;

// Constants
const DEFAULT_ACTIVE_KEY = '1';
const EXPAND_ICON_POSITION = 'right';

const AdminConsole = ({ blockUi }) => {
  return (
    <Container>
      <ReduxBlockUi
        tag="div"
        className="content-inner"
        block={REQUEST_START}
        unblock={[REQUEST_SUCCESS, REQUEST_FAILURE]}
        message={blockUi.message}
        loader={() => false}
        keepInView
      >
        <div className="sku-settings-content">
          <Collapse
            defaultActiveKey={DEFAULT_ACTIVE_KEY}
            expandIconPosition={EXPAND_ICON_POSITION}
            accordion
          >
            <Panel header={<b>User Management</b>} key="1">
              <PanelUserManagement />
            </Panel>
            <Panel header={<b>Data Source Management</b>} key="2">
              <Collapse
                defaultActiveKey={DEFAULT_ACTIVE_KEY}
                expandIconPosition={EXPAND_ICON_POSITION}
                accordion
              >
                <Panel header={<b>Data Sources</b>} key="1">
                  <PanelDataSourcesManagement />
                </Panel>
                <Panel header={<b>Schemas</b>} key="2">
                  <PanelSchemasManagement />
                </Panel>
              </Collapse>
            </Panel>
            <Panel header={<b>License Management</b>} key="3">
              <Collapse
                defaultActiveKey={DEFAULT_ACTIVE_KEY}
                expandIconPosition={EXPAND_ICON_POSITION}
                accordion
              >
                <Panel header={<b>License Information</b>} key="1">
                  <PanelLicenseManagement />
                </Panel>
                <Panel header={<b>Users List</b>} key="2">
                  <PanelLicenseUserManagement />
                </Panel>
              </Collapse>
            </Panel>
          </Collapse>
        </div>
      </ReduxBlockUi>
    </Container>
  );
};

AdminConsole.propTypes = {
  blockUi: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  blockUi: state.blockUi
});

export default connect(mapStateToProps)(AdminConsole);
