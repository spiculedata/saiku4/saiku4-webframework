/*
 *   Copyright 2012-present OSBI Ltd
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

// Packages
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Dialog } from '@blueprintjs/core';

// Forms
import { AdvancedForm, MondrianForm, XMLAForm } from './Forms';

// Styles
import './DataSourceFormDialog.css';

// Constants
const CONNECTION_TYPE_MONDRIAN = 'MONDRIAN';
const CONNECTION_TYPE_XMLA = 'XMLA';

class DataSourceFormDialog extends Component {
  state = {
    connectionType: 'MONDRIAN'
  };

  componentWillMount() {
    const { editMode, dataSourceData } = this.props;

    if (editMode) {
      if (dataSourceData.connectiontype === 'MONDRIAN') {
        this.setState({ connectionType: dataSourceData.connectiontype });
      } else if (dataSourceData.connectiontype === 'XMLA') {
        this.setState({ connectionType: dataSourceData.connectiontype });
      } else {
        this.setState({ connectionType: null });
      }
    }
  }

  handleChangeConnType = connectionType => {
    this.setState({ connectionType });
  };

  render() {
    const { editMode, dataSourceData, getDataSources, onClose } = this.props;
    const { connectionType } = this.state;

    return (
      <Dialog
        className="sku-datasource-form-dialog"
        title={!editMode ? 'Add Data Source' : 'Edit Data Source'}
        icon={!editMode ? 'plus' : 'edit'}
        canOutsideClickClose={false}
        isOpen={true}
        onClose={onClose}
      >
        {connectionType === CONNECTION_TYPE_MONDRIAN ? (
          <MondrianForm
            editMode={editMode}
            connType={this.handleChangeConnType}
            dataSourceData={dataSourceData}
            getDataSources={getDataSources}
            onClose={onClose}
          />
        ) : connectionType === CONNECTION_TYPE_XMLA ? (
          <XMLAForm
            editMode={editMode}
            connType={this.handleChangeConnType}
            dataSourceData={dataSourceData}
            getDataSources={getDataSources}
            onClose={onClose}
          />
        ) : (
          <AdvancedForm
            editMode={editMode}
            connType={this.handleChangeConnType}
            dataSourceData={dataSourceData}
            getDataSources={getDataSources}
            onClose={onClose}
          />
        )}
      </Dialog>
    );
  }
}

DataSourceFormDialog.propTypes = {
  editMode: PropTypes.bool,
  dataSourceData: PropTypes.object,
  getDataSources: PropTypes.func.isRequired,
  onClose: PropTypes.func.isRequired
};

DataSourceFormDialog.defaultProps = {
  editMode: false
};

export default DataSourceFormDialog;
