/*
 *   Copyright 2012-present OSBI Ltd
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

// Packages
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import classnames from 'classnames';
import { Grid, Row, Col } from 'react-flexbox-grid';
import { AnchorButton, Button, Intent } from '@blueprintjs/core';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';

// UI
import { Logo } from '../../../UI';

// Dialogs
import { ShowEEMessageDialog } from '../../../Dialogs';

// Utils
import { Links } from '../../../../utils';

// Images
import drawingWelcome from '../../../../images/drawings/drawing-01.svg';

// Styles
const styles = theme => ({
  root: {
    padding: '20px',
    flexGrow: 1
  },
  skuSectionAbout: {
    marginBottom: '40px'
  },
  skuSectionTitle: {
    color: '#6d6e71',
    fontWeight: 'bold',
    marginBottom: '12px'
  },
  skuButton: {
    fontSize: '13px !important'
  }
});

class TabWelcome extends PureComponent {
  state = {
    isShowEEMessageDialog: false
  };

  handleShowEEMessageDialog = () => {
    this.setState(prevState => ({
      isShowEEMessageDialog: !prevState.isShowEEMessageDialog
    }));
  };

  render() {
    const { classes, history } = this.props;
    const { isShowEEMessageDialog } = this.state;

    return (
      <div className={classes.root}>
        <Grid>
          <Row>
            {/* Left Section */}
            <Col xs>
              {/* Section Logo */}
              <Row>
                <Col xs>
                  <Logo width={250} height={125} full />
                </Col>
              </Row>
              {/* Section About */}
              <Row className={classes.skuSectionAbout}>
                <Col xs>
                  <Typography align="justify">
                    Saiku has the power to change the way you think about your
                    business and make decisions. Saiku provides powerful, web
                    based analytics for everyone in your organisation. Quickly
                    and easily analyse data from any data source to discover
                    what is really happening inside and outside your
                    organisation.
                  </Typography>
                </Col>
              </Row>
              {/* Section Title */}
              <Row>
                <Col xs>
                  <Typography
                    className={classes.skuSectionTitle}
                    variant="button"
                  >
                    Quick Links
                  </Typography>
                </Col>
              </Row>
              {/* Section Buttons */}
              <Row className="m-b-20">
                <Col xs={8}>
                  <Row>
                    <Col xs>
                      <Button
                        className={classnames('m-b-15', classes.skuButton)}
                        text="Create a New Query"
                        onClick={() => history.push('/workspace')}
                        fill
                        large
                      />
                    </Col>
                    <Col xs>
                      <Button
                        className={classnames('m-b-15', classes.skuButton)}
                        text="Create a Dashboard"
                        onClick={this.handleShowEEMessageDialog}
                        fill
                        large
                      />
                    </Col>
                  </Row>
                  <Row>
                    <Col xs>
                      <AnchorButton
                        className={classes.skuButton}
                        href={Links.meteorite.products.saiku.home}
                        text="Visit the Website"
                        target="_blank"
                        fill
                        large
                      />
                    </Col>
                    <Col xs>
                      <AnchorButton
                        className={classes.skuButton}
                        href={Links.saiku.docs.tutorials}
                        text="Tutorials"
                        target="_blank"
                        fill
                        large
                      />
                    </Col>
                  </Row>
                </Col>
              </Row>
              {/* Section Get Enterprise */}
              <Row>
                <Col xs={8}>
                  <AnchorButton
                    intent={Intent.DANGER}
                    href={Links.meteorite.home}
                    text="Get Enterprise"
                    target="_blank"
                    fill
                    large
                  />
                </Col>
              </Row>
            </Col>
            {/* Right Section */}
            <Col xs>
              <img src={drawingWelcome} alt="Office Saiku drawing" />
            </Col>
          </Row>
        </Grid>

        {isShowEEMessageDialog && (
          <ShowEEMessageDialog onClose={this.handleShowEEMessageDialog} />
        )}
      </div>
    );
  }
}

TabWelcome.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(withRouter(TabWelcome));
