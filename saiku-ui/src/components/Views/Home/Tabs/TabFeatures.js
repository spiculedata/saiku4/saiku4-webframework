/*
 *   Copyright 2012-present OSBI Ltd
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

// Packages
import React from 'react';
import PropTypes from 'prop-types';
import { Grid, Row, Col } from 'react-flexbox-grid';
import { Collapse } from 'antd';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';

// UI
import { Logo } from '../../../UI';

// Components
const Panel = Collapse.Panel;

// Styles
const styles = theme => ({
  root: {
    padding: '20px',
    flexGrow: 1
  },
  skuSectionTitle: {
    color: '#6d6e71',
    fontWeight: 'bold',
    marginBottom: '12px'
  },
  skuCollapse: {
    backgroundColor: '#fafafa'
  }
});

const TabFeatures = ({ classes }) => {
  return (
    <div className={classes.root}>
      <Grid>
        <Row>
          <Col xs>
            {/* Section Logo */}
            <Row>
              <Col xs>
                <Logo width={250} height={125} full />
              </Col>
            </Row>
            {/* Section Title */}
            <Row>
              <Col xs>
                <Typography
                  className={classes.skuSectionTitle}
                  variant="button"
                >
                  Features
                </Typography>
              </Col>
            </Row>
            {/* Section Features */}
            <Row>
              <Col xs>
                <Collapse
                  className={classes.skuCollapse}
                  bordered={false}
                  accordion
                >
                  <Panel header={<b>Web Based Analysis</b>} key="1">
                    <Typography className="m-b-10" align="justify">
                      Saiku provides the user with an entirely browser based
                      experience. We support all modern browsers, and our user
                      interface is 100% HTML and Javascript.
                    </Typography>
                    <Typography align="justify">
                      Saiku uses REST based communications, this allows the
                      development of custom user interfaces and facilitates the
                      easy integration of the Saiku Server into other
                      applications and services.
                    </Typography>
                  </Panel>
                  <Panel header={<b>Standards Compliant</b>} key="2">
                    <Typography align="justify">
                      Saiku is based upon the Microsoft MDX query language and
                      will work on most JDBC compliant data sources. We also
                      provide a number of connectors to NOSQL data sources.
                    </Typography>
                  </Panel>
                  <Panel header={<b>Dynamic Charting</b>} key="3">
                    <Typography align="justify">
                      Saiku uses a flexible charting engine to provide a wide
                      range of charts and graphs. These are all HTML &
                      Javascript only and don't require flash to be installed on
                      the computer.
                    </Typography>
                  </Panel>
                  <Panel header={<b>Pluggable Visualisation Engine</b>} key="4">
                    <Typography align="justify">
                      Saiku Enterprise boasts a fully pluggable visualisation
                      engine. This allows developers to build third party
                      extensions and plug them into Saiku Enterprise to extend
                      or replace the existing visualisations.
                    </Typography>
                  </Panel>
                </Collapse>
              </Col>
            </Row>
          </Col>
        </Row>
      </Grid>
    </div>
  );
};

TabFeatures.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(TabFeatures);
