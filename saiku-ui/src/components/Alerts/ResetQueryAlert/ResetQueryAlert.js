/*
 *   Copyright 2012-present OSBI Ltd
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

// Packages
import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Alert, Intent } from '@blueprintjs/core';

// Actions
import { actionCreators } from '../../../actions';

const ResetQueryAlert = props => {
  const { newQuery, onCancel } = props;

  const handleResetQuery = () => {
    newQuery();
    onCancel();
  };

  return (
    <Alert
      canEscapeKeyCancel={true}
      canOutsideClickCancel={true}
      cancelButtonText="Cancel"
      confirmButtonText="Clear"
      icon="info-sign"
      intent={Intent.DANGER}
      isOpen={true}
      onCancel={onCancel}
      onConfirm={handleResetQuery}
    >
      <p>You are about to clear your existing query!</p>
    </Alert>
  );
};

ResetQueryAlert.propTypes = {
  newQuery: PropTypes.func.isRequired,
  onCancel: PropTypes.func.isRequired
};

const mapDispatchToProps = dispatch => ({
  newQuery: () => dispatch(actionCreators.newQuery())
});

export default connect(
  null,
  mapDispatchToProps
)(ResetQueryAlert);
