/*
 *   Copyright 2012-present OSBI Ltd
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

// Packages
import React from 'react';
import PropTypes from 'prop-types';
import { Spinner } from '@blueprintjs/core';
import classnames from 'classnames';

const Loading = ({ className, intent, text, size, center, style }) => {
  return (
    <div
      className={classnames({
        'center-block': center
      })}
      {...style}
    >
      <Spinner className={className} intent={intent} size={size} /> &nbsp;{' '}
      {text}
    </div>
  );
};

Loading.propTypes = {
  className: PropTypes.string,
  intent: PropTypes.oneOf(['danger', 'none', 'primary', 'success', 'warning']),
  text: PropTypes.string,
  size: PropTypes.number,
  center: PropTypes.bool
};

Loading.defaultProps = {
  intent: 'danger',
  size: 30
};

export default Loading;
