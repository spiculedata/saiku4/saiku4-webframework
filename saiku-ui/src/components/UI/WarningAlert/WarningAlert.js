/*
 *   Copyright 2012-present OSBI Ltd
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

// Packages
import React from 'react';
import PropTypes from 'prop-types';
import { Alert, Intent } from '@blueprintjs/core';

const WarningAlert = props => {
  const {
    cancelButtonText,
    confirmButtonText,
    icon,
    message,
    onConfirm,
    onCancel
  } = props;

  const handleConfirm = () => {
    onConfirm();
    onCancel();
  };

  return (
    <Alert
      canEscapeKeyCancel={true}
      canOutsideClickCancel={true}
      cancelButtonText={cancelButtonText}
      confirmButtonText={confirmButtonText}
      icon={icon}
      intent={Intent.DANGER}
      isOpen={true}
      onCancel={onCancel}
      onConfirm={handleConfirm}
    >
      <p>{message}</p>
    </Alert>
  );
};

WarningAlert.propTypes = {
  cancelButtonText: PropTypes.string,
  confirmButtonText: PropTypes.string,
  icon: PropTypes.string,
  message: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
  onCancel: PropTypes.func.isRequired,
  onConfirm: PropTypes.func
};

WarningAlert.defaultProps = {
  cancelButtonText: 'Cancel',
  confirmButtonText: 'Okay',
  icon: 'info-sign'
};

export default WarningAlert;
