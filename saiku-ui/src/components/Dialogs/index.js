/*
 *   Copyright 2012-present OSBI Ltd
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

// Dialogs
import AboutDialog from './AboutDialog';
import CubesDialog from './CubesDialog';
import EnterLicenseDialog from './EnterLicenseDialog';
import IssueReporterDialog from './IssueReporterDialog';
import KeyboardShortcutsDialog from './KeyboardShortcutsDialog';
import OpenQueryDialog from './OpenQueryDialog';
import QueryDesignerDialog from './QueryDesignerDialog';
import SaveQueryDialog from './SaveQueryDialog';
import ShowEEMessageDialog from './ShowEEMessageDialog';
import ShowMdxDialog from './ShowMdxDialog';
import ViewFileInfoDialog from './ViewFileInfoDialog';

export {
  AboutDialog,
  CubesDialog,
  EnterLicenseDialog,
  IssueReporterDialog,
  KeyboardShortcutsDialog,
  OpenQueryDialog,
  QueryDesignerDialog,
  SaveQueryDialog,
  ShowEEMessageDialog,
  ShowMdxDialog,
  ViewFileInfoDialog
};
