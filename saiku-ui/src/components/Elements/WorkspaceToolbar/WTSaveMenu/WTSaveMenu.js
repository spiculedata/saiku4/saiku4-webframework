/*
 *   Copyright 2012-present OSBI Ltd
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

// Packages
import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Menu, MenuItem } from '@blueprintjs/core';

// Actions
import { actionCreators } from '../../../../actions';

const WTSaveMenu = props => {
  const handleSaveQuery = () => {
    const { workspace, saveQueryDialog, saveQueryAuto } = props;
    const { file } = workspace;

    if (file) {
      saveQueryAuto(file);
    } else {
      saveQueryDialog();
    }
  };

  const handleSaveQueryDialog = () => {
    props.saveQueryDialog();
  };

  return (
    <Menu>
      <MenuItem text="Save" label="Ctrl+S" onClick={handleSaveQuery} />
      <MenuItem
        text="Save as"
        label="Ctrl+Shift+S"
        onClick={handleSaveQueryDialog}
      />
    </Menu>
  );
};

WTSaveMenu.propTypes = {
  workspace: PropTypes.object.isRequired,
  saveQueryDialog: PropTypes.func.isRequired,
  saveQueryAuto: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  ...state.workspace
});

const mapDispatchToProps = dispatch => ({
  saveQueryAuto: file => dispatch(actionCreators.saveQueryAuto(file))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(WTSaveMenu);
