/*
 *   Copyright 2012-present OSBI Ltd
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

// Packages
import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { NavLink } from 'react-router-dom';
import { Icon } from '@blueprintjs/core';

// Utils
import { Saiku } from '../../../utils';

// Styles
import './Navigation.css';

const Navigation = ({ user }) => {
  const { isadmin } = user;

  return (
    <div className="sku-navigation">
      <ul>
        <li>
          <NavLink to="/settings/general" activeClassName="active">
            <Icon icon="cog" title={false} /> <span>General Settings</span>
          </NavLink>
        </li>
        <li>
          <NavLink to="/settings/querymanager" activeClassName="active">
            <Icon icon="folder-open" title={false} /> <span>Query Manager</span>
          </NavLink>
        </li>
        {isadmin && !Saiku.isSaikuDemo() && (
          <li>
            <NavLink
              to="/settings/adminconsole"
              exact={true}
              activeClassName="active"
            >
              <Icon icon="console" title={false} /> <span>Admin Console</span>
            </NavLink>
          </li>
        )}
      </ul>
    </div>
  );
};

Navigation.propTypes = {
  user: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  ...state.session
});

export default connect(mapStateToProps)(Navigation);
