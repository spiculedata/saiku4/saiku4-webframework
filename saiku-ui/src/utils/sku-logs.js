/*
 *   Copyright 2012-present OSBI Ltd
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

export const skuCELogMsg = () => {
  // Source: http://patorjk.com/software/taag/#p=display&f=ANSI%20Shadow&t=SAIKU%204
  console.log(
    `%c

███████╗ █████╗ ██╗██╗  ██╗██╗   ██╗    ██╗  ██╗
██╔════╝██╔══██╗██║██║ ██╔╝██║   ██║    ██║  ██║
███████╗███████║██║█████╔╝ ██║   ██║    ███████║
╚════██║██╔══██║██║██╔═██╗ ██║   ██║    ╚════██║
███████║██║  ██║██║██║  ██╗╚██████╔╝         ██║
╚══════╝╚═╝  ╚═╝╚═╝╚═╝  ╚═╝ ╚═════╝          ╚═╝

Next Generation Open Source Analytics
________________________________________________________________________________________________________________

Get a Saiku Enterprise license!

Saiku Enterprise delivers straightforward data analysis for everyone.

Our cost effective pricing model supports every scenario from single users to entire enterprises.

To take advantage of all the features of Saiku Enterprise every user requires a licence.

All Enterprise users are treated equally and covered by a single license type.

With Saiku 4 it is possible to use Community Edition to deliver Enterprise content when purchasing a minimum
number of Enterprise licences. Annual subscription Licences are available to purchase online for immediate use.
Payment can be made in US Dollars, Euros and GB Pounds or in your local currency. Please contact us to enquire
about perpetual licencing and other payment options.

https://www.meteorite.bi/products/saiku-pricing
________________________________________________________________________________________________________________

Contacts:

It doesn't matter where you are in the world, we are always happy to hear from people.
Call us or email us, we'll be happy to discuss your requirements.

  - Address: Surrey Technology Centre, Surrey Research Park, Guildford, GU2 7YG, UK
  - Phone: +44 20 8133 3730/+1.844.814.1689
  - Email: info@meteorite.bi
`,
    'font-family: monospace; color: #c52120; font-size: 12px;'
  );
};
