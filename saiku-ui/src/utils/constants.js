/*
 *   Copyright 2012-present OSBI Ltd
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

// Constants
export const REQUEST_START = 'REQUEST_START';
export const REQUEST_SUCCESS = 'REQUEST_SUCCESS';
export const REQUEST_FAILURE = /fail/i;
export const MEASURES = 'MEASURES';
export const ROWS = 'ROWS';
export const COLUMNS = 'COLUMNS';
export const FILTER = 'FILTER';
export const ROW_HEADER = 'ROW_HEADER';
export const ROW_HEADER_HEADER = 'ROW_HEADER_HEADER';
export const COLUMN_HEADER = 'COLUMN_HEADER';
export const FOLDER = 'FOLDER';
export const FILE = 'FILE';
export const PRIVATE = 'PRIVATE';
export const SECURED = 'SECURED';
export const READ = 'READ';
export const WRITE = 'WRITE';
export const GRANT = 'GRANT';
