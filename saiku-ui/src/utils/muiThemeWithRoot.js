/*
 *   Copyright 2012-present OSBI Ltd
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

// Packages
import React from 'react';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';

// Utils
import { Settings } from './Settings';

// A theme with custom primary and secondary color
const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#1b1a1e',
      contrastText: '#fff'
    },
    secondary: {
      main: Settings.SAIKU_COLOR,
      contrastText: '#fff'
    }
  },
  typography: {
    useNextVariants: true
  }
});

function muiThemeWithRoot(Component) {
  function MuiWithRoot(props) {
    // MuiThemeProvider makes the theme available down the React tree
    return (
      <MuiThemeProvider theme={theme}>
        {/* CssBaseline kickstart an elegant, consistent, and simple baseline to build upon. */}
        <CssBaseline />
        <Component {...props} />
      </MuiThemeProvider>
    );
  }

  return MuiWithRoot;
}

export default muiThemeWithRoot;
