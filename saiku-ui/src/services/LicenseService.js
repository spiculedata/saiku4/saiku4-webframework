/*
 *   Copyright 2012-present OSBI Ltd
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

// Packages
import axios from 'axios';
import qs from 'qs';

// Utils
import { Saiku, Settings } from '../utils';

// Constants
const LICENSE_URL = 'api/license';

// Axios Cancellation
const CancelToken = axios.CancelToken;
let axiosCancelRequest;

class LicenseService {
  static async getLicense() {
    const url = `${Settings.REST_URL}/${LICENSE_URL}`;
    let response;

    try {
      response = await axios.get(url, {
        cancelToken: new CancelToken(function executor(c) {
          // An executor function receives a cancel function as a parameter
          axiosCancelRequest = c;
        })
      });
    } catch (error) {
      Saiku.axiosHandleErrors(
        'services → LicenseService.js → getLicense()',
        error
      );

      return error.response;
    }

    return response;
  }

  static async getLicenseUsers() {
    const url = `${Settings.REST_URL}/${LICENSE_URL}/users`;
    let response;

    try {
      response = await axios.get(url, {
        cancelToken: new CancelToken(function executor(c) {
          // An executor function receives a cancel function as a parameter
          axiosCancelRequest = c;
        })
      });
    } catch (error) {
      Saiku.axiosHandleErrors(
        'services → LicenseService.js → getLicenseUsers()',
        error
      );

      return error.response;
    }

    return response;
  }

  static async postLicense(licenseData) {
    const url = `${Settings.REST_URL}/${LICENSE_URL}`;
    let response;

    try {
      response = await axios.post(url, qs.stringify(licenseData), {
        headers: {
          'content-type': 'application/x-www-form-urlencoded'
        },
        cancelToken: new CancelToken(function executor(c) {
          // An executor function receives a cancel function as a parameter
          axiosCancelRequest = c;
        })
      });
    } catch (error) {
      Saiku.axiosHandleErrors(
        'services → LicenseService.js → postLicense()',
        error
      );

      return error.response;
    }

    return response;
  }

  static async postLicenseUser(name) {
    const url = `${Settings.REST_URL}/${LICENSE_URL}/users`;
    const data = [];
    let response;

    data.push({
      name
    });

    try {
      response = await axios.post(url, JSON.stringify(data), {
        headers: {
          'content-type': 'application/json'
        },
        cancelToken: new CancelToken(function executor(c) {
          // An executor function receives a cancel function as a parameter
          axiosCancelRequest = c;
        })
      });
    } catch (error) {
      Saiku.axiosHandleErrors(
        'services → LicenseService.js → postLicenseUser()',
        error
      );

      return error.response;
    }

    return response;
  }

  static cancelRequest() {
    // Cancel the axios request
    // Reference: https://github.com/axios/axios#cancellation
    axiosCancelRequest();
  }
}

export default LicenseService;
