/*
 *   Copyright 2012-present OSBI Ltd
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

// Packages
const gulp = require('gulp');
const uglify = require('gulp-uglify');
const concat = require('gulp-concat');
const path = require('path');

// Constants
const SOURCE_FOLDER = 'ccc_chart';
const DEST_FOLDER = 'public/js/libs/ccc_chart';

gulp.task('default', () => {
  return gulp.src([
    path.resolve(__dirname, `${SOURCE_FOLDER}/jquery.tipsy.js`),
    path.resolve(__dirname, `${SOURCE_FOLDER}/protovis.js`),
    path.resolve(__dirname, `${SOURCE_FOLDER}/protovis-msie.js`),
    path.resolve(__dirname, `${SOURCE_FOLDER}/tipsy.js`),
    path.resolve(__dirname, `${SOURCE_FOLDER}/def.js`),
    path.resolve(__dirname, `${SOURCE_FOLDER}/cdo.js`),
    path.resolve(__dirname, `${SOURCE_FOLDER}/pvc.js`)
  ])
  .pipe(concat('ccc_chart.min.js'))
  .pipe(uglify())
  .pipe(gulp.dest(DEST_FOLDER));
});
