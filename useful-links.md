# Useful Links

## React API

- [Using Context API in React (Hooks and Classes)](https://www.taniarascia.com/using-context-api-in-react/)

## Immutable Patterns

- [Immutable Update Patterns](https://redux.js.org/recipes/structuring-reducers/immutable-update-patterns)
- [Immutably setting a value in a JS array (or how an array is also an object)](https://medium.com/@giltayar/immutably-setting-a-value-in-a-js-array-or-how-an-array-is-also-an-object-55337f4d6702)
- [ES6 Spread Immutable Cheatsheet](https://gist.github.com/gorangajic/e902c2ee994260b3348d)

## Performance

- [React.js: reduce your javascript bundle with code splitting](https://medium.com/kaliop/react-js-reduce-your-javascript-bundle-with-code-splitting-f2d24abd42b8)

## Redux connect in React HOC

- [Connected Higher Order Component (HOC)](https://medium.com/practo-engineering/connected-higher-order-component-hoc-93ee63c91526)
- [Use connect on HOC components](https://github.com/reduxjs/react-redux/issues/837)
- [Use Redux connect in React HOC](https://stackoverflow.com/questions/51535377/use-redux-connect-in-react-hoc)

## Error Boundaries

- [Introducing Error Boundaries](https://reactjs.org/docs/error-boundaries.html)
- [React 16 Error Boundaries](https://blog.sentry.io/2017/09/28/react-16-error-boundaries)

## Others

- [Environments in Create React App](https://serverless-stack.com/chapters/environments-in-create-react-app.html)
