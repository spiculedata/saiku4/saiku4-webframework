/*
 *   Copyright 2012-present OSBI Ltd
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

{
  datasources: [
    {
      uniqueName: 'earthquakes',
      name: 'earthquakes',
      catalogs: [
        {
          uniqueName: 'Global Earthquakes',
          name: 'Global Earthquakes',
          schemas: [
            {
              uniqueName: 'Global Earthquakes',
              name: 'Global Earthquakes',
              cubes: [
                {
                  uniqueName:
                    '[earthquakes].[Global Earthquakes].[Global Earthquakes].[Earthquakes]',
                  name: 'Earthquakes',
                  connection: 'earthquakes',
                  catalog: 'Global Earthquakes',
                  schema: 'Global Earthquakes',
                  caption: 'Earthquakes',
                  visible: true
                }
              ]
            }
          ]
        }
      ]
    },
    {
      uniqueName: 'foodmart',
      name: 'foodmart',
      catalogs: [
        {
          uniqueName: 'FoodMart',
          name: 'FoodMart',
          schemas: [
            {
              uniqueName: 'FoodMart',
              name: 'FoodMart',
              cubes: [
                {
                  uniqueName: '[foodmart].[FoodMart].[FoodMart].[HR]',
                  name: 'HR',
                  connection: 'foodmart',
                  catalog: 'FoodMart',
                  schema: 'FoodMart',
                  caption: 'HR',
                  visible: true
                },
                {
                  uniqueName: '[foodmart].[FoodMart].[FoodMart].[Sales]',
                  name: 'Sales',
                  connection: 'foodmart',
                  catalog: 'FoodMart',
                  schema: 'FoodMart',
                  caption: 'Sales',
                  visible: true
                },
                {
                  uniqueName: '[foodmart].[FoodMart].[FoodMart].[Sales 2]',
                  name: 'Sales 2',
                  connection: 'foodmart',
                  catalog: 'FoodMart',
                  schema: 'FoodMart',
                  caption: 'Sales 2',
                  visible: true
                },
                {
                  uniqueName: '[foodmart].[FoodMart].[FoodMart].[Store]',
                  name: 'Store',
                  connection: 'foodmart',
                  catalog: 'FoodMart',
                  schema: 'FoodMart',
                  caption: 'Store',
                  visible: true
                },
                {
                  uniqueName: '[foodmart].[FoodMart].[FoodMart].[Warehouse]',
                  name: 'Warehouse',
                  connection: 'foodmart',
                  catalog: 'FoodMart',
                  schema: 'FoodMart',
                  caption: 'Warehouse',
                  visible: true
                },
                {
                  uniqueName:
                    '[foodmart].[FoodMart].[FoodMart].[Warehouse and Sales]',
                  name: 'Warehouse and Sales',
                  connection: 'foodmart',
                  catalog: 'FoodMart',
                  schema: 'FoodMart',
                  caption: 'Warehouse and Sales',
                  visible: true
                }
              ]
            }
          ]
        }
      ]
    }
  ]
}
