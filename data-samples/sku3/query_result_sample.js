/*
 *   Copyright 2012-present OSBI Ltd
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

{
  cellset: [
    [
      {
        value: 'null',
        type: 'COLUMN_HEADER',
        properties: {}
      },
      {
        value: 'F',
        type: 'COLUMN_HEADER',
        properties: {
          uniquename: '[Customer].[Gender].[F]',
          hierarchy: '[Customer].[Gender]',
          dimension: 'Customer',
          level: '[Customer].[Gender].[Gender]'
        }
      },
      {
        value: 'F',
        type: 'COLUMN_HEADER',
        properties: {
          uniquename: '[Customer].[Gender].[F]',
          hierarchy: '[Customer].[Gender]',
          dimension: 'Customer',
          level: '[Customer].[Gender].[Gender]'
        }
      },
      {
        value: 'F',
        type: 'COLUMN_HEADER',
        properties: {
          uniquename: '[Customer].[Gender].[F]',
          hierarchy: '[Customer].[Gender]',
          dimension: 'Customer',
          level: '[Customer].[Gender].[Gender]'
        }
      },
      {
        value: 'M',
        type: 'COLUMN_HEADER',
        properties: {
          uniquename: '[Customer].[Gender].[M]',
          hierarchy: '[Customer].[Gender]',
          dimension: 'Customer',
          level: '[Customer].[Gender].[Gender]'
        }
      },
      {
        value: 'M',
        type: 'COLUMN_HEADER',
        properties: {
          uniquename: '[Customer].[Gender].[M]',
          hierarchy: '[Customer].[Gender]',
          dimension: 'Customer',
          level: '[Customer].[Gender].[Gender]'
        }
      },
      {
        value: 'M',
        type: 'COLUMN_HEADER',
        properties: {
          uniquename: '[Customer].[Gender].[M]',
          hierarchy: '[Customer].[Gender]',
          dimension: 'Customer',
          level: '[Customer].[Gender].[Gender]'
        }
      }
    ],
    [
      {
        value: 'Product Family',
        type: 'ROW_HEADER_HEADER',
        properties: {
          hierarchy: '[Product].[Products]',
          dimension: 'Product',
          level: '[Product].[Products].[Product Family]'
        }
      },
      {
        value: 'Unit Sales',
        type: 'COLUMN_HEADER',
        properties: {
          uniquename: '[Measures].[Unit Sales]',
          hierarchy: '[Measures]',
          dimension: 'Measures',
          level: '[Measures].[MeasuresLevel]'
        }
      },
      {
        value: 'Store Cost',
        type: 'COLUMN_HEADER',
        properties: {
          uniquename: '[Measures].[Store Cost]',
          hierarchy: '[Measures]',
          dimension: 'Measures',
          level: '[Measures].[MeasuresLevel]'
        }
      },
      {
        value: 'Store Sales',
        type: 'COLUMN_HEADER',
        properties: {
          uniquename: '[Measures].[Store Sales]',
          hierarchy: '[Measures]',
          dimension: 'Measures',
          level: '[Measures].[MeasuresLevel]'
        }
      },
      {
        value: 'Unit Sales',
        type: 'COLUMN_HEADER',
        properties: {
          uniquename: '[Measures].[Unit Sales]',
          hierarchy: '[Measures]',
          dimension: 'Measures',
          level: '[Measures].[MeasuresLevel]'
        }
      },
      {
        value: 'Store Cost',
        type: 'COLUMN_HEADER',
        properties: {
          uniquename: '[Measures].[Store Cost]',
          hierarchy: '[Measures]',
          dimension: 'Measures',
          level: '[Measures].[MeasuresLevel]'
        }
      },
      {
        value: 'Store Sales',
        type: 'COLUMN_HEADER',
        properties: {
          uniquename: '[Measures].[Store Sales]',
          hierarchy: '[Measures]',
          dimension: 'Measures',
          level: '[Measures].[MeasuresLevel]'
        }
      }
    ],
    [
      {
        value: 'Drink',
        type: 'ROW_HEADER',
        properties: {
          uniquename: '[Product].[Products].[Drink]',
          hierarchy: '[Product].[Products]',
          dimension: 'Product',
          level: '[Product].[Products].[Product Family]'
        }
      },
      {
        value: '12,202',
        type: 'DATA_CELL',
        properties: {
          position: '0:0',
          raw: '12202.0'
        }
      },
      {
        value: '9,751.10',
        type: 'DATA_CELL',
        properties: {
          position: '1:0',
          raw: '9751.0984'
        }
      },
      {
        value: '24,457.37',
        type: 'DATA_CELL',
        properties: {
          position: '2:0',
          raw: '24457.37'
        }
      },
      {
        value: '12,395',
        type: 'DATA_CELL',
        properties: {
          position: '3:0',
          raw: '12395.0'
        }
      },
      {
        value: '9,726.14',
        type: 'DATA_CELL',
        properties: {
          position: '4:0',
          raw: '9726.1362'
        }
      },
      {
        value: '24,378.84',
        type: 'DATA_CELL',
        properties: {
          position: '5:0',
          raw: '24378.84'
        }
      }
    ],
    [
      {
        value: 'Food',
        type: 'ROW_HEADER',
        properties: {
          uniquename: '[Product].[Products].[Food]',
          hierarchy: '[Product].[Products]',
          dimension: 'Product',
          level: '[Product].[Products].[Product Family]'
        }
      },
      {
        value: '94,814',
        type: 'DATA_CELL',
        properties: {
          position: '0:1',
          raw: '94814.0'
        }
      },
      {
        value: '80,993.45',
        type: 'DATA_CELL',
        properties: {
          position: '1:1',
          raw: '80993.4515'
        }
      },
      {
        value: '203,094.17',
        type: 'DATA_CELL',
        properties: {
          position: '2:1',
          raw: '203094.17'
        }
      },
      {
        value: '97,126',
        type: 'DATA_CELL',
        properties: {
          position: '3:1',
          raw: '97126.0'
        }
      },
      {
        value: '82,277.27',
        type: 'DATA_CELL',
        properties: {
          position: '4:1',
          raw: '82277.272'
        }
      },
      {
        value: '205,941.42',
        type: 'DATA_CELL',
        properties: {
          position: '5:1',
          raw: '205941.42'
        }
      }
    ],
    [
      {
        value: 'Non-Consumable',
        type: 'ROW_HEADER',
        properties: {
          uniquename: '[Product].[Products].[Non-Consumable]',
          hierarchy: '[Product].[Products]',
          dimension: 'Product',
          level: '[Product].[Products].[Product Family]'
        }
      },
      {
        value: '24,542',
        type: 'DATA_CELL',
        properties: {
          position: '0:2',
          raw: '24542.0'
        }
      },
      {
        value: '21,032.93',
        type: 'DATA_CELL',
        properties: {
          position: '1:2',
          raw: '21032.9291'
        }
      },
      {
        value: '52,674.67',
        type: 'DATA_CELL',
        properties: {
          position: '2:2',
          raw: '52674.67'
        }
      },
      {
        value: '25,694',
        type: 'DATA_CELL',
        properties: {
          position: '3:2',
          raw: '25694.0'
        }
      },
      {
        value: '21,846.35',
        type: 'DATA_CELL',
        properties: {
          position: '4:2',
          raw: '21846.3464'
        }
      },
      {
        value: '54,691.66',
        type: 'DATA_CELL',
        properties: {
          position: '5:2',
          raw: '54691.66'
        }
      }
    ]
  ],
  rowTotalsLists: null,
  colTotalsLists: null,
  runtime: 99,
  error: null,
  height: 5,
  width: 7,
  topOffset: 2,
  leftOffset: 0
}
